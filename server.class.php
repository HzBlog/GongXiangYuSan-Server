<?php
require 'Config.php';
class Server{
	protected $serv;
	private static $instance;
	private function __construct(){
		$this->serv = new swoole_websocket_server(WEB_HOST,WEB_PORT);
		$this->serv->addlistener(TCP_HOST,TCP_PORT,SWOOLE_TCP)->set(array(
			'worker_num'=>4,
			'log_file'=>"./swoole.log",
			'daemonize'=>true,
			'backlog'=>128,
			'open_eof_check'=>true,
			'package_eof'=>'\r\n\r\n',
		)) ;
	}

	public static function getInstance(){
		if(empty(self::$instance)){
			self::$instance = new Server;
		}
		return self::$instance;
	}
	
	/*
	*在work/task进程启动时发生
	*/
	public function onWorkerStart($workstart){
		$this->serv->on('WorkerStart',$workstart);
	}

	/*
	*tcp服务连接回调
	*/
	public function onConnect($connect){
		$this->serv->on('connect',$connect);
	}

	/*
	*tcp服务接收消息回调
	*/
	public function onReceive($receive){
		$this->serv->on('receive',$receive);
	}

	/*
	*tcp和websocket服务共用连接断开回调
	*/
	public function onClose($close){
		$this->serv->on('close',$close);
	}
	
	/*
	*websocket服务连接回调
	*/
	public function onOpen($open){
		$this->serv->on('open',$open);
	}
	
	/*
	*websocket服务消息接收回调
	*/
	public function onMessage($message){
		$this->serv->on('message',$message);
	}

	/*
	*整个服务关闭了，
	*/
	public function onShutdown($shutdown){
		$this->serv->on('shutdown',$shutdown);
	}

	public function start(){
		$this->serv->start();
	}
}


