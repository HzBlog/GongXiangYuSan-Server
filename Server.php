<?php
require "redis.class.php";
require "server.class.php";
require "Mysql.class.php";
$Serv = Server::getInstance();
//$Cache = redisCache::getInstance();
$connect = function (swoole_server $Serv,$fd,$reactor_id){
	
};
$workstart = function (swoole_server $Serv,$worker_id){
	$Serv->redis = new redisCache();
	$Serv->mysql = new Mysql();
};

/*
*Tcp消息接收回调
*1.实现硬件和websocket第一次连接后保存对应硬件和描述符的保存:
*	数据格式: 硬件->服务:'length,xxxxxxxxx\r\n\r\n'
*		  web前端->服务：'length,电话号码\r\n\r\n'
*2.还伞时候通信操作:
*	数据格式: 
*		还伞：web->服务：数据格式:‘length,1,XXXXXXX,X,还伞电话\r\n\r\n’  ;服务->硬件：‘length,x,1,还伞电话’
*		硬件->服务: 数据格式:'length,3,XXXXXXX,X,1,还伞电话\r\n\r\n' 还伞成功    'length,3,XXXXXXX,X,0,还伞电话\r\n\r\n' 还伞失败
*		服务->phpWeb前端:
*				‘1,还伞成功’
*				‘0,还伞失败，请联系客服’
*3.借伞时候通信操作:
*	数据格式: 
*		借伞：web->服务: 数据格式:'length,0,xxxxxxx,x,借伞电话\r\n\r\n' ;服务->硬件：‘length，x，0，借伞电话’ 
*		硬件确认：硬件->服务：‘length,2,xxxxxxx,x,1，借伞电话\r\n\r\n’     伞架口操作正常啦，借伞成功 ;  ‘length,2,xxxxxxx,x,0，借伞电话\r\n\r\n’     伞架口操作异常啦，借伞失败 ;
*		服务确认：服务->phpWeb前端：
*					‘1,借伞成功’
*					‘0,借伞失败，请联系客服。’		
*4.数据格式参数：
*	'length'->后面所有数据长度，除去逗号;  'XXXXXXXX'->伞架ID;  'X->伞架口ID ;
*	数字：关于前端：1成功 0失败  ;关于硬件：0借伞 1还伞 2借伞成功确认 3借伞失败确认
*/
$receive = function (swoole_server $Serv,$fd,$reactor_id,$data){
	$arr = explode(',',trim($data));
	$strCheckLength = (int)$arr[0];
	$strLength = 0;
	for($i = 1;$i<count($arr);$i++){
		$arr[$i]=trim($arr[$i]);
		$strLength+=strlen($arr[$i]);
	}
	if($strCheckLength == $strLength){
		if(count($arr)==2){        			//伞架第一次接入
			$Serv->redis->setTcpVal($arr[1],$fd);
		}else{						
			if($arr[1]==1){				//还伞操作
				$SendFd = $Serv->redis->getTcpVal($arr[2]);	
				if($SendFd!=0){
					$SendLeng = strlen($arr[3])+strlen($arr[4])+1;
					$SendRe = $Serv->send($SendFd,"$SendLeng,$arr[3],1,$arr[4]");
					if($SendRe){
						$re = $Serv->mysql->updata('umb_rackport',['rackPort_status'=>'2'])->where(['rackPort_rackPortId'=>$arr[3],'rackPort_rackId'=>$arr[2]])->query()->rows();
						//数据库中对应伞架口状态改为等待还伞
						
					}else{
						//硬件断开连接
						file_put_contents("./error.log",date('Y-m-d H:i:s').':伞架:'.$arr[2].'通信异常,还伞请求失败001'."\r\n",FILE_APPEND);
					}	
				}else{
						//对应连接断开
						file_put_contents("./error.log",date('Y-m-d H:i:s').':伞架'.$arr[2].'连接断开,还伞请求失败002'."\r\n",FILE_APPEND);
				}		
				
			}else if($arr[1] == 3){			//还伞确认操作
				if($arr[4] == 1){
					//数据库中对应伞架口状态改变为已还伞
					$PushFd = $Serv->redis->getWebVal($arr[5]);
					echo $PushFd;
					if($PushFd!=0){
						$Serv->mysql->updata('umb_rackport',['rackPort_status'=>'1'])->where(['rackPort_rackPortId'=>$arr[3],'rackPort_rackId'=>$arr[2]])->query()->rows();
						$Serv->push($PushFd,'1,还伞成功。');
					}else{
						//对应链接断开
						//file_put_contents("./error.log",date('Y-m-d H:i:s').':websocket连接断开,换伞成功，未向用户反反馈'."\r\n",FILE_APPEND);
						file_put_contents("./error.log",date('Y-m-d H:i:s').':'.$arr[5].':websocket连接断开,伞架:'.$arr[2].';伞架口:'.$arr[3].';还伞成功，但未向用户反馈'."\r\n",FILE_APPEND);
					}
					
				}else{
					//数据库中对应伞架口状态改为未还伞
					$PushFd = $Serv->redis->getWebVal($arr[5]);
					if($PushFd!=0){
						$Serv->mysql->updata('umb_rackport',['rackPort_status'=>'0'])->where(['rackPort_rackPortId'=>$arr[3],'rackPort_rackId'=>$arr[2]])->query()->rows();
						$Serv->push($PushFd,'0,还伞失败，请联系客服。');
					}else{
						//对应链接断开
						//file_put_contents("./error.log",date('Y-m-d H:i:s').':websocket连接断开,换伞失败，未向用户反反馈'."\r\n",FILE_APPEND);
						file_put_contents("./error.log",date('Y-m-d H:i:s').':'.$arr[5].':websocket连接断开,伞架:'.$arr[2].';伞架口:'.$arr[3].';还伞失败，但未向用户反馈'."\r\n",FILE_APPEND);

					}
				}
			}else if($arr[1]==0){			//借伞请求操作
				$SendFd = $Serv->redis->getTcpVal($arr[2]);	
				if($SendFd!=0){
					$SendLeng = strlen($arr[3])+strlen($arr[4])+1;
					$SendRe = $Serv->send($SendFd,"$SendLeng,$arr[3],0,$arr[4]");
					if($SendRe){
						$Serv->mysql->updata('umb_rackport',['rackPort_status'=>'2'])->where(['rackPort_rackPortId'=>$arr[3],'rackPort_rackId'=>$arr[2]])->query()->rows();
						//数据库中对应伞架口状态改为等待借出
					}else{
						//硬件断开连接
						file_put_contents("./error.log",date('Y-m-d H:i:s').':伞架:'.$arr[2].'通信异常,借伞请求失败001'."\r\n",FILE_APPEND);
					}	
				}else{
					//对应连接断开
					file_put_contents("./error.log",date('Y-m-d H:i:s').':伞架:'.$arr[2].'通信异常,借伞请求失败002'."\r\n",FILE_APPEND);
				}
			}else if($arr[1]==2){			//借伞确认操作
				if($arr[4] == 1){
					//数据库中对应伞架口状态改变为已借出
					$PushFd = $Serv->redis->getWebVal($arr[5]);
					if($PushFd!=0){
						$re = $Serv->mysql->updata('umb_rackport',['rackPort_status'=>'0'])->where(['rackPort_rackPortId'=>$arr[3],'rackPort_rackId'=>$arr[2]])->query()->rows();
						$Serv->push($PushFd,'1,借伞成功。');
					}else{
						//对应连接断开	
						file_put_contents("./error.log",date('Y-m-d H:i:s').':'.$arr[5].':websocket连接断开,伞架:'.$arr[2].';伞架口:'.$arr[3].';被打开，但未向用户反馈'."\r\n",FILE_APPEND);
					}
				}else{
					//数据库中对应伞架口状态改为未借出
					$PushFd = $Serv->redis->getWebVal($arr[5]);
					if($PushFd!=0){
						$re = $Serv->mysql->updata('umb_rackport',['rackPort_status'=>'1'])->where(['rackPort_rackPortId'=>$arr[3],'rackPort_rackId'=>$arr[2]])->query()->rows();
						file_put_contents("./error.log",date('Y-m-d H:i:s').':伞架:'.$arr[2].';伞架口:'.$arr[3].';硬件反馈借伞失败'."\r\n",FILE_APPEND);
						$Serv->push($PushFd,'2,借伞失败，请联系客服。');
					}else{
						//对应连接断开	
						file_put_contents("./error.log",date('Y-m-d H:i:s').':'.$arr[5].':websocket连接断开,伞架:'.$arr[2].';伞架口:'.$arr[3].';硬件反馈借伞失败'."\r\n",FILE_APPEND);
					}
				}
			}		
		}
	}else{
		echo "send message error!\r\n";
		file_put_contents("./error.log",date('Y-m-d H:i:s').':'.$data.'：数据格式错误'."\r\n",FILE_APPEND);
		//写进通信数据错误日志
	}	
};
$open = function (swoole_websocket_server $Serv,$request){

};

/*
*websocket消息接收回调
*websocket连接时，以手机号为k，websocket链接描述符为V存入连接到redis
*/
$message = function (swoole_websocket_server $Serv,$frame){
	$arr = explode(',',trim($frame->data));
	if(count($arr)==2 && ((int)$arr[0]==strlen($arr[1]))){
		$Serv->redis->setWebVal($arr[1],$frame->fd);
	}
};
$close = function (swoole_server $Serv,$fd,$reactor_id){
	$Serv->redis->delWebVal($fd);	
	$Serv->redis->delTcpVal($fd);
};
$shutdown = function (swoole_server $Serv){
	$Serv->redis->delall();
};
$Serv->onWorkerStart($workstart);	
$Serv->onConnect($connect);
$Serv->onReceive($receive);
$Serv->onOpen($open);
$Serv->onMessage($message);
$Serv->onClose($close);
$Serv->onShutdown($shutdown);
$Serv->start();
